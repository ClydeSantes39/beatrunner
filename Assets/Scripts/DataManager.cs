﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CI.QuickSave;

public class DataManager : MonoBehaviour
{
    static DataManager _data;

    public static DataManager instance {
        get {
            if (_data == null)
                _data = FindObjectOfType<DataManager>();

            return _data;
        }
    } 

    public void Save() {
        QuickSaveWriter.Create("Data")
                       .Write("High Score", TextManager.instance.highScore)
                       .Commit();
    }

    public void Load() {
        QuickSaveReader.Create("Data")
                        .Read<int>("High Score", (r) => {TextManager.instance.highScore = r;});
    }
}
