﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class UnityEventExtension
{

    private static EventMessenger GlobalMessenger = EventMessenger.Instance;
    private static Dictionary<Type, List<string>> GlobalMessengerListeners = new Dictionary<Type, List<string>>();
    private static Dictionary<GameObject, EventMessenger> Messengers = new Dictionary<GameObject, EventMessenger>();
    private static readonly bool LOG_ENABLED = false;

    #region GameObject
    public static void RaiseEvent<T>(this GameObject obj, T e) where T : GameEvent
    {
        EventMessenger em;
        if (!Messengers.TryGetValue(obj, out em))
        {
            em = new EventMessenger();
            Messengers[obj] = em;
        }
        em.Raise<T>(e);

        if (UnityEventExtension.LOG_ENABLED)
        {
            Debug.Log(obj.name + "<" + typeof(T).Name + "> Event raised");
        }
    }

    public static void RaiseEventGlobal<T>(this GameObject obj, T e) where T : GameEvent
    {
        GlobalMessenger.Raise<T>(e);
    }

    public static void AddEventListener<T>(this GameObject obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        EventMessenger em;
        if (!Messengers.TryGetValue(obj, out em))
        {
            em = new EventMessenger();
            Messengers[obj] = em;
        }
        em.AddListener<T>(del);

        if (UnityEventExtension.LOG_ENABLED)
        {
            Debug.Log(obj.name + "<" + typeof(T).Name + "> Listener added [" + del.Target.GetType().Name + " " + del.Method.Name + "]");
        }
    }

    public static void AddEventListenerGlobal<T>(this GameObject obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        GlobalMessenger.AddListener<T>(del);
    }

    public static void RemoveEventListener<T>(this GameObject obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        EventMessenger em;
        if (!Messengers.TryGetValue(obj, out em)) return;


        em.RemoveListener<T>(del);

        if (UnityEventExtension.LOG_ENABLED)
        {
            Debug.Log(obj.name + "<" + typeof(T).Name + "> Listener removed [" + del.Target.GetType().Name + " " + del.Method.Name + "]");
        }
    }

    public static void RemoveEventListeners(this GameObject obj)
    {
        Messengers.Remove(obj);

        if (UnityEventExtension.LOG_ENABLED)
        {
            Debug.Log(obj.name + "<???> All listeners removed");
        }
    }

    public static void RemoveEventListenerGlobal<T>(this GameObject obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        GlobalMessenger.RemoveListener<T>(del);
    }
    #endregion

    #region MonoBehaviour
    public static void RaiseEvent<T>(this MonoBehaviour obj, T e) where T : GameEvent
    {
        GameObject go = obj.gameObject;
        go.RaiseEvent<T>(e);
    }

    public static void RaiseEventGlobal<T>(this MonoBehaviour obj, T e) where T : GameEvent
    {
        List<string> listeners;
        string raiseMessage = obj.GetType().ToString() + " of " + obj.gameObject.name + "Raise the Event " + e.GetType().ToString() + "\n The following are currently listening: \n";

        if (GlobalMessengerListeners.TryGetValue(typeof(T), out listeners))
        {
            for (int i = 0; i < listeners.Count; i++)
            {
                raiseMessage += listeners[i];
            }
        }

        //Debug.Log(raiseMessage);

        GlobalMessenger.Raise<T>(e);
    }

    public static void AddEventListener<T>(this MonoBehaviour obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        GameObject go = obj.gameObject;
        go.AddEventListener<T>(del);
    }

    public static void AddEventListenerGlobal<T>(this MonoBehaviour obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        List<string> listeners;
        string listenerInfo = obj.GetType().ToString() + " of " + obj.gameObject.name + "\n";


        if (GlobalMessengerListeners.TryGetValue(typeof(T), out listeners))
        {
            listeners.Add(listenerInfo);
            GlobalMessengerListeners[typeof(T)] = listeners;
        }
        else
        {
            listeners = new List<string>();
            listeners.Add(listenerInfo);
            GlobalMessengerListeners[typeof(T)] = listeners;
        }


        GlobalMessenger.AddListener<T>(del);
    }

    public static void RemoveEventListener<T>(this MonoBehaviour obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        GameObject go = obj.gameObject;
        go.RemoveEventListener<T>(del);
    }

    public static void RemoveEventListeners(this MonoBehaviour obj)
    {
        Messengers.Remove(obj.gameObject);
    }

    public static void RemoveEventListenerGlobal<T>(this MonoBehaviour obj, EventMessenger.EventDelegate<T> del) where T : GameEvent
    {
        List<string> listeners;
        string listenerInfo = obj.GetType().ToString() + " of " + obj.gameObject.name + "\n";

        if (GlobalMessengerListeners.TryGetValue(typeof(T), out listeners))
        {
            listeners.Remove(listenerInfo);

            if (listeners.Count == 0)
            {
                GlobalMessengerListeners.Remove(typeof(T));
            }
            else
            {
                GlobalMessengerListeners[typeof(T)] = listeners;
            }
        }


        GlobalMessenger.RemoveListener<T>(del);
    }
    #endregion
}
