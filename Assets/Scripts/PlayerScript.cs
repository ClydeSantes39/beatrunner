﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerScript : MonoBehaviour
{
    static PlayerScript _player;

    public static PlayerScript instance {
        get {
            if (_player == null)
                _player = FindObjectOfType<PlayerScript>();

            return _player;
        }
    } 

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    //Animates player to move
    public void PlayerJump() {
        this.GetComponent<Transform>().DOLocalJump(new Vector3(-5, 0, 0), 2, 1, 0.45f, false);
    }
}
