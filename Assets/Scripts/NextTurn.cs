﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DG.Tweening;

public class NextTurn : MonoBehaviour
{
    /*public*/private Sprite movePrompt;
    private Sprite jumpPrompt;
    private SpriteRenderer sprRender;
    public GameObject obstacle;
    private bool destroyPrefab;

    static NextTurn _next;

    public static NextTurn instance {
        get {
            if (_next == null)
                _next = FindObjectOfType<NextTurn>();

            return _next;
        }
    } 

    // Start is called before the first frame update
    void Start()
    {
        movePrompt = Resources.Load<Sprite>("Sprites/MovePrompt");
        jumpPrompt = Resources.Load<Sprite>("Sprites/JumpPrompt");

        sprRender = GetComponent<SpriteRenderer>();
        MovePrompt();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Spawns move turn if called
    public void MovePrompt() {
        sprRender.sprite = movePrompt;
    }

    //Spawns obstacle (jump turn) called
    public void JumpPrompt() {
        //Creates obstacle prefab
        obstacle = Instantiate(Resources.Load("Prefabs/Obstacle"), 
                        new Vector3(9, 0, 0), 
                        Quaternion.identity) as GameObject;

        //Animates it to move into position    
        obstacle.GetComponent<Transform>().DOLocalMoveX(4, 0.5f, false);
    }

    //Called if Player has Jumped
    public void JumpPromptDone() {
        //Animates it to move out of camera view
        obstacle.GetComponent<Transform>().DOLocalMoveX(-10, 0.5f, false);
        //Tags it as Done or has exited the screen
        obstacle.tag = "Done";
    }
}
