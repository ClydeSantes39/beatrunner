﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject inGameUI;
    public GameObject mainMenu;
    public GameObject gameOver;

    static UIManager _ui;
    public static UIManager instance {
        get {
            if (_ui == null)
                _ui = FindObjectOfType<UIManager>();

            return _ui;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        inGameUI = GameObject.Find("In-Game UI");
        mainMenu = GameObject.Find("Main Menu");
        gameOver = GameObject.Find("Game Over");

        inGameUI.SetActive(false);
        mainMenu.SetActive(true);
        gameOver.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestartGame() {
        gameOver.SetActive(false);
        mainMenu.SetActive(true);
        GameManagerScript.instance.currentPattern.Clear();
        Destroy(NextTurn.instance.obstacle);
        
    }
}
