﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string[] sceneNames;

    // Start is called before the first frame update
    void Start()
    {
        for(int idx = 0; idx < sceneNames.Length; idx++) {
            if (!SceneManager.GetSceneByName(sceneNames[idx]).isLoaded) {
                SceneManager.LoadScene(sceneNames[idx], LoadSceneMode.Additive);
            }
        }
    }
}
