﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    static TextManager _text;
    public static TextManager instance {
        get {
            if (_text == null)
                _text = FindObjectOfType<TextManager>();

            return _text;
        }
    }

    public int currentScore;
    public int highScore;
    private Text scoreText;
    private Text highscoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GameObject.Find("Score Text").GetComponent<Text>();
        highscoreText = GameObject.Find("HighScore Text").GetComponent<Text>();

        currentScore = 0;

        if (highScore.Equals(null)) {
            highScore = 0;
        } else {
            DataManager.instance.Load();
        }
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score  " + currentScore;
        highscoreText.text = "High Score   " + highScore;
    }

    public void SaveHighScore() {
        if (currentScore > highScore) {
            highScore = currentScore;
        }
    }
}
