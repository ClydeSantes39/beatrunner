﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hellmade.Sound;

public class ButtonManager : MonoBehaviour
{
    private AudioClip movePromptAudio;
    private AudioClip jumpPromptAudio;

    void Start() {
        movePromptAudio = Resources.Load<AudioClip>("Audio/Bass Drum Sound Effect");
        jumpPromptAudio = Resources.Load<AudioClip>("Audio/Snare Drum Hit Sound Effect");
    }

    void Update()
    {

    }

    public void MoveButton() {
        if (GameManagerScript.instance.GetCurrentIndex() == 1){     //Checks if move button was pressed
            GameManagerScript.instance.GameOver();                  //even if there's an obstacle
            DataManager.instance.Save();
        } else {
            GameManagerScript.instance.timeLeft = 2;                //Refills time after every move
            GameManagerScript.instance.IncrementList();             //Calls for next turn on List

            EazySoundManager.PlaySound(movePromptAudio);            //Plays Move audio (bass drum)
        }
    }

    public void JumpButton() {
        if (GameManagerScript.instance.GetCurrentIndex() == 1){     //Checks if Jump Button was pressed
            TextManager.instance.currentScore++;                           //Adds score if jumped on obstacle
            GameManagerScript.instance.timeLeft = 2;                //Refills time if jumped on obstacle
        }

        PlayerScript.instance.PlayerJump();                         //Calls PlayerScript to play Jump animation
        NextTurn.instance.JumpPromptDone();                         //Calls NextTurn to signify a Jump has been made
        GameManagerScript.instance.IncrementList();                 //Calls for next turn on List

        EazySoundManager.PlaySound(jumpPromptAudio);                //Plays Jump audio (snare drum)
    }
}
