﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManagerScript : MonoBehaviour
{
    static GameManagerScript _game;
    public static GameManagerScript instance { 
        get {
            if (_game == null) {
                _game = FindObjectOfType<GameManagerScript>();
            }
            return _game;
        }
    }

    private int arrayNum;
    private int currentIndex;

    //Each beat patterns. 0 means Move Prompt. 1 means Jump Prompt. Extra 0 at the end safeguards against any
    //spawning errors of next turns
    private int[][] beatPatterns = new int[3][]{new int[]{0,0,0,1,0},new int[]{0,1,0,1,0}, new int[]{0,0,1,1,0}};

    private int repeat;
    private int nextSet;
    private GameObject obstacle;
    public int timeLeft;
    public bool isGameOver;

    public List<int> currentPattern = new List<int>();             //Signifies the beat pattern currently playing

    void Start()
    {
        //DataManager.instance.Load();
    }

    void Update()
    {
        //Checks if current beat pattern is complete. If so, it repeats it back to first beat
        //and adds to number of repeats 
        if (arrayNum == 4) {
            arrayNum = 0;
            repeat++;
        }

        //Triggers Game Over if time runs out
        if (timeLeft < 0 && isGameOver == false) {
            isGameOver = true;
            GameOver();
        }
    }

    public void NewGame() {
        arrayNum = 0;                                       //Signifies the current beat
        repeat = 0;                                         //Signifies the current no. of repeats of a pattern
        nextSet = 0;                                        //Signifies the next set of beat patterns
        timeLeft = 2;
        currentIndex = 0;
        isGameOver = false;

        StartCoroutine("LoseTime");                         //Starts coroutine to gradually lose time.This allows beats to stay in rhythm
    }

    //This function calls for the next new set of beat patterns
    public void IncrementList() {

        //This finds any prefab obstacles that have exited, and destroy them to avoid data clutter
        if (GameObject.FindGameObjectsWithTag("Done") != null) {
            GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Done");     //Places done prefab objects into array

            for(int x=0; x < obstacles.Length; x++) {                               
                if (obstacles[x].transform.position.x == -10)       //Finds if obstacle prefab is in the -10 x position
                    Destroy(obstacles[x]);                          //Destroys said obstacle    
            }
        }

        //This happens if a set of pattern has been repeated twice
        if (repeat == 2) {
            repeat = 0;                                             //Resets num of repeats to 0
            nextSet = Random.Range(0,3);                            //Calls on next set randomly
            currentPattern.Clear();                                 //Clears the current beat pattern in the List
        }

        currentPattern = new List<int>(beatPatterns[nextSet]);      //Places next set into current pattern

        arrayNum++;                     
        currentIndex = currentPattern[arrayNum];                    //Places current beat pattern num as 
                                                                    //current index to be analyzed       

        if (currentIndex == 0) {
            NextTurn.instance.MovePrompt();                         //Calls NextTurn to spawn Move Prompt if index is 0
        } else if (currentIndex == 1) {
            NextTurn.instance.JumpPrompt();                         //Calls NextTurn to spawn Jump Prompt (Obstacle) if index is 0
        }    
    }

    public int GetArrayNum() {
        return arrayNum;
    }

    public int GetCurrentIndex() {
        return currentIndex;
    }

    //Triggers Game Over
    public void GameOver() {
        StopCoroutine("LoseTime");
        //Time.timeScale = 0;
        
        //DataManager.instance.Save();
        TextManager.instance.SaveHighScore();
        DataManager.instance.Save();
        TextManager.instance.currentScore = 0;
        UIManager.instance.inGameUI.SetActive(false);
        UIManager.instance.gameOver.SetActive(true);
        //Set GameOver to Active here    
    }
    
    //Gradually loses time for a beat
    IEnumerator LoseTime() {
        while (true) {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
